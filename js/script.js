// Loader

document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
        // document.getElementById('content').style.visibility = "hidden";
        $('.content').hide('fast');
    } else if (state == 'complete') {
        setTimeout(function () {
            // document.getElementById('interactive');
                    $('#interactive');
            // document.getElementById('loader').style.visibility = "hidden";
                    $('.loader').css("visibility", "hidden").fadeOut('slow');
                    $('.content').css("visibility", "visible").fadeIn('slow');

            // document.getElementById('content').style.visibility = "visible";
        }, 3000);
    }
}

// FULLPAGE.js
$(document).ready(function () {
    console.log("fullpage ready!");
    $(document).ready(function () {
        $('#fullpage').fullpage();
    });

    $(document).ready(function () {
        $('#fullpage').fullpage({
            anchors: ['contact', 'about']

        });
    });

    $('#fullpage').fullpage({
        scrollOverflow: true,
        verticalCentered: false
    });

    $(window).load(function () {
        $('#cover').fadeOut(1000);

    });
});

// Chart.js


$(document).ready(function () {

    Chart.defaults.global.defaultFontColor = '#f1c40f';

    var ctx = document.getElementById("skill-chart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Java", "Python", "HTML", "CSS", "JS", "C#"],
            datasets: [{
                label: 'Percentage Knowledge',
                data: [70, 30, 85, 75, 50, 20],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1,
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMax: 100,
                        beginAtZero: true


                    },
                    scaleLabel: {
                        display: true,
                        labelString: '% Knowledge',
                        fontColor: '#f5f5f5'

                    }
                }],
                xAxes: [{

                    scaleLabel: {
                        display: true,
                        labelString: 'Programming Languages',
                        fontColor: '#f5f5f5'
                    }
                }]
            },
            legend: {
                display: false,
            }

        }
    });
});

$(document).ready(function() {
    
});